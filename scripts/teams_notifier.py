from adaptivecardbuilder import AdaptiveCard, TextBlock, ColumnSet
import json
import requests
from requests.structures import CaseInsensitiveDict

'''
test via https://reqbin.com/
adaptive cards for python
https://github.com/ku222/AdaptiveCardBuilder
'''


class TeamsNotifier:
    def __init__(self, options={}):
        assert 'url' in options
        self.url = options['url']
        if 'template' not in options:
            self.template = """
                            {
                               "type": "message",
                               "attachments": [
                                  {
                                     "contentType": "application/vnd.microsoft.card.adaptive",
                                     "contentUrl": "null",
                                     "content": "null"
                                  }
                               ]
                            }
                            """
        else:
            self.template = options['template']
        if 'card' in options:
            self.card = options['card']
        else:
            self.card = None

    async def send(self, message):
        data = json.loads(self.template)
        if not self.card:
            data['attachments'][0]['content'] = await self.get_card(message)
        else:
            data['attachments'][0]['content'] = self.card
        headers = CaseInsensitiveDict()
        headers["Content-Type"] = "application/json"
        print(self.card)
        resp = requests.post(self.url, headers=headers,
                             data=json.dumps(data))
        return resp.status_code

    async def get_card(self, message):
        # initialize card
        card = AdaptiveCard()
        # Add a textblock
        card.add(TextBlock(text=message, separator="true", spacing="large"))
        # add column set
        card.add(ColumnSet())
        return await card.to_dict()



