#!/usr/bin/env bash
# /etc/azcreds/azlogin.sh

export EXPERIMENT_FOLDER=$USER_CLUSTER_NAME-$(date +"%F-%H-%M-%S")
export RESOURCE_NAME=$EXPERIMENT_FOLDER
envsubst < $SPEC_PATH/aci_spec.yaml > _aci_spec.yaml
azcopy cp "$(pwd)/*" "https://$ACI_PERS_STORAGE_ACCOUNT_NAME.file.core.windows.net/$ACI_PERS_SHARE_NAME/tmp/$EXPERIMENT_FOLDER/?$SAS_TOKEN"  --recursive=true --exclude-path="output;.git;shared"
az container create --no-wait  -g $ACI_PERS_RESOURCE_GROUP --file _aci_spec.yaml
sleep 10
state="container"
echo Container has been created, monitoring is avaiable here https://portal.azure.com/#@austyuzhaninhse.onmicrosoft.com/resource/subscriptions/14df2028-ca5c-4b88-95f9-3a746a5d372a/resourceGroups/$ACI_PERS_RESOURCE_GROUP/providers/Microsoft.ContainerInstance/containerGroups/$RESOURCE_NAME/overview

while :
do
  vm=$( az container show -g $ACI_PERS_RESOURCE_GROUP -n $RESOURCE_NAME  --output table)
  while read -r line
  do
    name=$(echo $line | cut -f 1 -d ' ')
    if [ $name ==  $RESOURCE_NAME ]; then
       status=$(echo $line | cut -f 3 -d ' ')
       if [ $state != $status ]; then
         state=$status
         echo container $name changed status to $status
       fi
    fi
  done <<< "$vm"

  if [ $status == "Running" ]; then
    break
  fi
sleep 1
done

echo Container started, fetching logs
completed=0
while :
do
    #ms az util likes to crash without any exitcode - so we need some Indian code here
    az container logs --follow -g $ACI_PERS_RESOURCE_GROUP -n $RESOURCE_NAME
    vm=$( az container show -g $ACI_PERS_RESOURCE_GROUP -n $RESOURCE_NAME  --output table)
    while read -r line
    do
      name=$(echo $line | cut -f 1 -d ' ')
      if [ $name ==  $RESOURCE_NAME ]; then
         status=$(echo $line | cut -f 3 -d ' ')
         if [ $status != "Running" ]; then
           completed=1
           break
         fi
      fi
    done <<< "$vm"
if [ $completed == 1 ]; then
  break
fi
sleep 10
continue
done

azcopy cp  "https://$ACI_PERS_STORAGE_ACCOUNT_NAME.file.core.windows.net/$ACI_PERS_SHARE_NAME/tmp/$EXPERIMENT_FOLDER/output/*?$SAS_TOKEN" ./output/ --recursive=true
az container delete -g $ACI_PERS_RESOURCE_GROUP -n $RESOURCE_NAME -y

