from teams_notifier import TeamsNotifier
import os
import asyncio
from adaptivecardbuilder import AdaptiveCard, TextBlock, ColumnSet, Column, Image

if __name__ == '__main__':
    # simple variant
    options = {
        'url': 'https://eduhseru.webhook.office.com/webhookb2/'
               'bdab3281-8ac1-45e1-b7be-eb370ff51bde@21f26c24'
               '-0793-4b07-a73d-563cd2ec235f/IncomingWebhook/'
               'b1930b8669684126b71435c1ff5a7e0f/a138fef1-7074'
               '-4610-8915-165a5923e131',
    }
    notifier = TeamsNotifier(options)
    run_name = os.getenv('ELYRA_RUN_NAME')
    asyncio.run(notifier.send(f'Pipeline {run_name} run finished [Kubeflow](https://kubeflow.coresearch.club/_/pipeline/?ns=digits#/runs)'))
    # in case of "asyncio.run() cannot be called from a running event loop" error
    # in Jypthon for testing purpose use next line (replace it with asyncio.run() for production)
    # await notifier.send(f'Pipeline {run_name} run finished')
    exit(0)

    # complicated variant
    run_name = os.getenv('ELYRA_RUN_NAME')
    # initialize card
    card = AdaptiveCard()
    # Add a textblock
    card.add(TextBlock(text=f"WOHOO {run_name} is finished!" , separator="true", spacing="large"))
    # add column set
    card.add(ColumnSet())
    # First column contents
    card.add(Column(width=2))
    card.add(TextBlock(text="BANK OF LINGFIELD BRANCH"))
    card.add(TextBlock(text="NE Branch", size="ExtraLarge", weight="Bolder"))
    card.add(TextBlock(text="4.2 stars", isSubtle='true', spacing="None"))
    card.add(TextBlock(text="Some review text for illustration",
                       size="Small"))

    # Back up to column set
    card.up_one_level()

    # Second column contents
    card.add(Column(width=1))
    card.add(Image(url="https://s17026.pcdn.co/wp-content/uploads/sites/9/2018/08/Business-bank-account-e1534519443766.jpeg"))    
    options = {
        'url': 'https://eduhseru.webhook.office.com/webhookb2/'
               'bdab3281-8ac1-45e1-b7be-eb370ff51bde@21f26c24'
               '-0793-4b07-a73d-563cd2ec235f/IncomingWebhook/'
               'b1930b8669684126b71435c1ff5a7e0f/a138fef1-7074'
               '-4610-8915-165a5923e131',
        'card': asyncio.run(card.to_dict())
    }
    notifier = TeamsNotifier(options)
    #await notifier.send(f'Pipeline {run_name} run finished')
    asyncio.run(notifier.send(f'Pipeline {run_name} run finished'))